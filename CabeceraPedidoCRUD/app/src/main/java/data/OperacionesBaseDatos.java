package data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;


import java.util.ArrayList;
import java.util.List;

import data.ContratoPedidos.CabecerasPedido;
import modelo.CabeceraPedido;
import data.ContratoPedidos.Clientes;
import data.ContratoPedidos.FormasPago;
import data.ContratoPedidos.CabecerasPedido;
import data.CabeceraSQLHelper.Tablas;
import modelo.Cliente;
import modelo.FormaPago;


/**
 * Clase auxiliar que implementa a {@link CabeceraSQLHelper} para llevar a cabo el CRUD
 * sobre las entidades existentes.
 */
public final class OperacionesBaseDatos {

    private static CabeceraSQLHelper baseDatos;

    private static OperacionesBaseDatos instancia = new OperacionesBaseDatos();

    public OperacionesBaseDatos(){

    }

    public SQLiteDatabase getDb() {
        return baseDatos.getWritableDatabase();
    }

    public static OperacionesBaseDatos obtenerInstancia(Context contexto) {
        if (baseDatos == null) {
            baseDatos = new CabeceraSQLHelper(contexto);
        }
        return instancia;
    }

    // [OPERACIONES_CABECERA_PEDIDO]
    public Cursor obtenerCabecerasPedidos() {
        SQLiteDatabase db = baseDatos.getReadableDatabase();

        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();

        builder.setTables(CABECERA_PEDIDO_JOIN_CLIENTE_Y_FORMA_PAGO);

        return builder.query(db, proyCabeceraPedido, null, null, null, null, null);
    }



    public String insertarCabeceraPedido(CabeceraPedido pedido) {
        SQLiteDatabase db = baseDatos.getWritableDatabase();

        // Generar Pk
        String idCabeceraPedido = CabecerasPedido.generarIdCabeceraPedido();

        ContentValues valores = new ContentValues();
        valores.put(CabecerasPedido.ID, idCabeceraPedido);
        valores.put(CabecerasPedido.FECHA, pedido.fecha);
        valores.put(CabecerasPedido.ID_CLIENTE, pedido.idCliente);
        valores.put(CabecerasPedido.ID_FORMA_PAGO, pedido.idFormaPago);

        // Insertar cabecera
        db.insertOrThrow(Tablas.CABECERA_PEDIDO, null, valores);

        return idCabeceraPedido;
    }

    public boolean actualizarCabeceraPedido(CabeceraPedido pedidoNuevo) {
        SQLiteDatabase db = baseDatos.getWritableDatabase();

        ContentValues valores = new ContentValues();
        valores.put(CabecerasPedido.FECHA, pedidoNuevo.fecha);
        valores.put(CabecerasPedido.ID_CLIENTE, pedidoNuevo.idCliente);
        valores.put(CabecerasPedido.ID_FORMA_PAGO, pedidoNuevo.idFormaPago);

        String whereClause = String.format("%s=?", CabecerasPedido.ID);
        String[] whereArgs = {pedidoNuevo.idCabeceraPedido};

        int resultado = db.update(Tablas.CABECERA_PEDIDO, valores, whereClause, whereArgs);

        return resultado > 0;
    }

    public int eliminarCabeceraPedido(String idCabeceraPedido) {
        SQLiteDatabase db = baseDatos.getWritableDatabase();

        String whereClause = CabecerasPedido.ID + "=?";
        String[] whereArgs = {idCabeceraPedido};

        int resultado = db.delete(Tablas.CABECERA_PEDIDO, whereClause, whereArgs);

        return resultado;
    }
    // [/OPERACIONES_CABECERA_PEDIDO]


    // [OPERACIONES_CLIENTE]
    public Cursor obtenerClientes() {
        SQLiteDatabase db = baseDatos.getReadableDatabase();

        String sql = String.format("SELECT * FROM %s", Tablas.CLIENTE);

        return db.rawQuery(sql, null);
    }

    public Cursor obtenerClienteporNombre(String nombres) {
        SQLiteDatabase db = baseDatos.getWritableDatabase();

        String selection = String.format("%s=?", Clientes.NOMBRES);
        String[] selectionArgs = {nombres};

        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        builder.setTables(CABECERA_PEDIDO_JOIN_CLIENTE_Y_FORMA_PAGO);

        String[] proyeccion = {
                Clientes.ID,
                Clientes.NOMBRES,
                Clientes.APELLIDOS,
                Clientes.TELEFONO};

        return builder.query(db, proyeccion, selection, selectionArgs, null, null, null);
    }

    public String insertarCliente(Cliente cliente) {
        SQLiteDatabase db = baseDatos.getWritableDatabase();

        // Generar Pk
        String idCliente = Clientes.generarIdCliente();

        ContentValues valores = new ContentValues();
        valores.put(Clientes.ID, idCliente);
        valores.put(Clientes.NOMBRES, cliente.nombres);
        valores.put(Clientes.APELLIDOS, cliente.apellidos);
        valores.put(Clientes.TELEFONO, cliente.telefono);

        return db.insertOrThrow(Tablas.CLIENTE, null, valores) > 0 ? idCliente : null;
    }

    public boolean actualizarCliente(Cliente cliente) {
        SQLiteDatabase db = baseDatos.getWritableDatabase();

        ContentValues valores = new ContentValues();
        valores.put(Clientes.NOMBRES, cliente.nombres);
        valores.put(Clientes.APELLIDOS, cliente.apellidos);
        valores.put(Clientes.TELEFONO, cliente.telefono);

        String whereClause = String.format("%s=?", Clientes.ID);
        final String[] whereArgs = {cliente.idCliente};

        int resultado = db.update(Tablas.CLIENTE, valores, whereClause, whereArgs);

        return resultado > 0;
    }

    public boolean eliminarCliente(String idCliente) {
        SQLiteDatabase db = baseDatos.getWritableDatabase();

        String whereClause = String.format("%s=?", Clientes.ID);
        final String[] whereArgs = {idCliente};

        int resultado = db.delete(Tablas.CLIENTE, whereClause, whereArgs);

        return resultado > 0;
    }
    // [/OPERACIONES_CLIENTE]

    // [OPERACIONES_FORMA_PAGO]
    public Cursor obtenerFormasPago() {
        SQLiteDatabase db = baseDatos.getReadableDatabase();

        String sql = String.format("SELECT * FROM %s", Tablas.FORMA_PAGO);

        return db.rawQuery(sql, null);
    }

    public String insertarFormaPago(FormaPago formaPago) {
        SQLiteDatabase db = baseDatos.getWritableDatabase();

        // Generar Pk
        String idFormaPago = FormasPago.generarIdFormaPago();

        ContentValues valores = new ContentValues();
        valores.put(FormasPago.ID, idFormaPago);
        valores.put(FormasPago.NOMBRE, formaPago.nombre);

        return db.insertOrThrow(Tablas.FORMA_PAGO, null, valores) > 0 ? idFormaPago : null;
    }

    public boolean actualizarFormaPago(FormaPago formaPago) {
        SQLiteDatabase db = baseDatos.getWritableDatabase();

        ContentValues valores = new ContentValues();
        valores.put(FormasPago.NOMBRE, formaPago.nombre);

        String whereClause = String.format("%s=?", FormasPago.ID);
        String[] whereArgs = {formaPago.idFormaPago};

        int resultado = db.update(Tablas.FORMA_PAGO, valores, whereClause, whereArgs);

        return resultado > 0;
    }

    public boolean eliminarFormaPago(String idFormaPago) {
        SQLiteDatabase db = baseDatos.getWritableDatabase();

        String whereClause = String.format("%s=?", FormasPago.ID);
        String[] whereArgs = {idFormaPago};

        int resultado = db.delete(Tablas.FORMA_PAGO, whereClause, whereArgs);

        return resultado > 0;
    }

    // [/OPERACIONES_FORMA_PAGO]

    // Añadir nueva CabeceraPedido
    public String addCabecera(CabeceraPedido cabeceraPedido) {
        SQLiteDatabase db = baseDatos.getWritableDatabase();
        String rtrn = null;
        // Generar Pk
        String idCabeceraPedido = CabecerasPedido.generarIdCabeceraPedido();

        ContentValues values = new ContentValues();
        values.put(CabecerasPedido.ID, idCabeceraPedido);
        values.put(CabecerasPedido.FECHA, cabeceraPedido.getFecha());
        values.put(CabecerasPedido.ID_CLIENTE, cabeceraPedido.getIdCliente());
        values.put(CabecerasPedido.ID_FORMA_PAGO, cabeceraPedido.getIdFormaPago());

        // Inserting Row
        rtrn = db.insertOrThrow(Tablas.CABECERA_PEDIDO, null, values) > 0 ? idCabeceraPedido : null;
        return rtrn;
    }

    // Obtener una CabeceraPedido
    public CabeceraPedido getCabeceraPedido(String id) {
        SQLiteDatabase db = baseDatos.getWritableDatabase();

        String selection = String.format("%s=?", CabecerasPedido.ID);
        String[] selectionArgs = {id};

        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        builder.setTables(BUILDCABECERA);

        String[] proyeccion = {
                CabecerasPedido.ID,
                CabecerasPedido.FECHA,
                CabecerasPedido.ID_CLIENTE,
                CabecerasPedido.ID_FORMA_PAGO};

        Cursor cursor = builder.query(db, proyeccion, selection, selectionArgs, null, null, null);

        if (cursor !=null && cursor.getCount() > 0)
        {
            cursor.moveToFirst();
            CabeceraPedido cabecera = new CabeceraPedido(cursor.getString(0),
                    cursor.getString(1), cursor.getString(2), cursor.getString(3));
            return cabecera;
        }
        return null;
    }

    // Obtener Todas las CabeceraPedido
    public List<CabeceraPedido> getAllCabeceras() {
        List<CabeceraPedido> cabeceraList = new ArrayList<CabeceraPedido>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + Tablas.CABECERA_PEDIDO;

        SQLiteDatabase db = baseDatos.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

      // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                CabeceraPedido cabecera = new CabeceraPedido();
                cabecera.setIdCabeceraPedido(cursor.getString(0));
                cabecera.setFecha(cursor.getString(1));
                cabecera.setIdCliente(cursor.getString(2));
                cabecera.setIdFormaPago(cursor.getString(3));
                // Adding country to list
                cabeceraList.add(cabecera);
            } while (cursor.moveToNext());
        }

        // return country list
        return cabeceraList;
    }

    // Actualizar una CabeceraPedido
    public int updateCabecera(CabeceraPedido cabecera) {
        SQLiteDatabase db = baseDatos.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(CabecerasPedido.FECHA, cabecera.getFecha());
        values.put(CabecerasPedido.ID_CLIENTE, cabecera.getIdCliente());
        values.put(CabecerasPedido.ID_FORMA_PAGO, cabecera.getIdFormaPago());

        // updating row
        return db.update(Tablas.CABECERA_PEDIDO, values, CabecerasPedido.ID + " = ?",
                new String[] { String.valueOf(cabecera.getIdCabeceraPedido()) });
    }

    // Borrar una CabeceraPedido
    public void deleteCabecera(CabeceraPedido cabecera) {
        SQLiteDatabase db = baseDatos.getWritableDatabase();
        db.delete(Tablas.CABECERA_PEDIDO, CabecerasPedido.ID + " = ?",
                new String[] { String.valueOf(cabecera.getIdCabeceraPedido()) });
        db.close();
    }

    // Borrar Todas las CabeceraPedido
    public void deleteAllCabeceras() {
        SQLiteDatabase db = baseDatos.getWritableDatabase();
        db.delete(Tablas.CABECERA_PEDIDO,null,null);
        db.close();
    }

    // Obtener la cuenta de CabeceraPedido
    public int getCabeceraCount() {
        String countQuery = "SELECT  * FROM " + Tablas.CABECERA_PEDIDO;
        SQLiteDatabase db = baseDatos.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }

    // Añadir nuevo Cliente
    public String addCliente(Cliente client) {
        SQLiteDatabase db = baseDatos.getWritableDatabase();
        String rtrn = null;
        // Generar Pk
        String idCliente = Clientes.generarIdCliente();

        ContentValues values = new ContentValues();
        values.put(Clientes.ID, idCliente);
        values.put(Clientes.NOMBRES, client.getNombres());
        values.put(Clientes.APELLIDOS, client.getApellidos());
        values.put(Clientes.TELEFONO, client.getTelefono());

        // Inserting Row
        rtrn =  db.insertOrThrow(Tablas.CLIENTE, null, values) > 0 ? idCliente : null;
        return rtrn;
    }

    // Obtener un Cliente
    public Cliente getCliente(String id) {
        SQLiteDatabase db = baseDatos.getWritableDatabase();

        String[] dateParts = id.split(" ");
        id = dateParts[0];

        String selection = String.format("%s=?", Clientes.NOMBRES);
        String[] selectionArgs = {id};

        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        builder.setTables(BUILDCLIENTE);

        String[] proyeccion = {
                Clientes.ID,
                Clientes.NOMBRES,
                Clientes.APELLIDOS,
                Clientes.TELEFONO};

        Cursor cursor = builder.query(db, proyeccion, selection, selectionArgs, null, null, null);

        if (cursor !=null && cursor.getCount() > 0)
        {
            cursor.moveToFirst();
            Cliente cliente = new Cliente(cursor.getString(0),
                cursor.getString(1), cursor.getString(2), cursor.getString(3));
            return cliente;
        }
        return null;
    }


    // Añadir nuevo Cliente
    public String addFormaPago(FormaPago formPago) {
        SQLiteDatabase db = baseDatos.getWritableDatabase();
        String rturn = null;
        // Generar Pk
        String idFormaPago = FormasPago.generarIdFormaPago();

        ContentValues values = new ContentValues();
        values.put(FormasPago.ID, idFormaPago);
        values.put(FormasPago.NOMBRE, formPago.getNombre());

        // Inserting Row
        rturn =  db.insertOrThrow(Tablas.FORMA_PAGO, null, values) > 0 ? idFormaPago : null;
        return rturn;
    }

    // Obtener una FormaPago
    public FormaPago getFormaPago(String id) {
        SQLiteDatabase db = baseDatos.getWritableDatabase();

        String selection = String.format("%s=?", FormasPago.NOMBRE);
        String[] selectionArgs = {id};

        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        builder.setTables(BUILDFORMAPAGO);

        String[] proyeccion = {
                FormasPago.ID,
                FormasPago.NOMBRE};

        Cursor cursor = builder.query(db, proyeccion, selection, selectionArgs, null, null, null);

        if (cursor !=null && cursor.getCount() > 0)
        {
            cursor.moveToFirst();
            FormaPago formaPago = new FormaPago(cursor.getString(0),
                    cursor.getString(1));
            return formaPago;
        }
        return null;
    }

    /**
     * Getting all Clients
     * returns list of Clients
     * */
    public List<String> getAllClients(){
        List<String> list = new ArrayList<String>();

        // Select All Query
        String selectQuery = "SELECT nombres  || ' '|| apellidos FROM " + Tablas.CLIENTE;

        SQLiteDatabase db = baseDatos.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);//selectQuery,selectedArguments

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                list.add(cursor.getString(0));//adding 2nd column data
            } while (cursor.moveToNext());
        }
        // closing connection
        cursor.close();
        db.close();
        // returning clients
        return list;
    }

    /**
     * Getting all Forma de Pago
     * returns list of Forma de Pago
     * */
    public List<String> getAllFormaPago(){
        List<String> list = new ArrayList<String>();

        // Select All Query
        String selectQuery = "SELECT nombre FROM " + Tablas.FORMA_PAGO;

        SQLiteDatabase db = baseDatos.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);//selectQuery,selectedArguments

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                list.add(cursor.getString(0));//adding 2nd column data
            } while (cursor.moveToNext());
        }
        // closing connection
        cursor.close();
        db.close();
        // returning clients
        return list;
    }

    private static final String CABECERA_PEDIDO_JOIN_CLIENTE_Y_FORMA_PAGO = "cabecera_pedido " +
            "INNER JOIN cliente " +
            "ON cabecera_pedido.id_cliente = cliente.id " +
            "INNER JOIN forma_pago " +
            "ON cabecera_pedido.id_forma_pago = forma_pago.id";

    private static final String BUILDCLIENTE = "cliente";

    private static final String BUILDFORMAPAGO = "forma_pago";

    private static final String BUILDCABECERA = "cabecera_pedido";


    private final String[] proyCabeceraPedido = new String[]{
            Tablas.CABECERA_PEDIDO + "." + CabecerasPedido.ID,
            CabecerasPedido.FECHA,
            Clientes.NOMBRES + " || ' '|| "+ Clientes.APELLIDOS,
            FormasPago.NOMBRE};

}
