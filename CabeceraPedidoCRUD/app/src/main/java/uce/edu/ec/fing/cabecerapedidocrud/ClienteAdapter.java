package uce.edu.ec.fing.cabecerapedidocrud;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.List;

import data.OperacionesBaseDatos;
import modelo.CabeceraPedido;
import modelo.Cliente;

public class ClienteAdapter extends ArrayAdapter<Cliente> {


    public ClienteAdapter(Context context, List<Cliente> objects) {super(context,0, objects);}


    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //getting employee of the specified position
        Cliente client = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_layout_cliente, parent, false);
        }

        //getting views
        TextView textViewID =  convertView.findViewById(R.id.tv_id_cliente);
        TextView textViewFecha =  convertView.findViewById(R.id.tv_nombres);
        TextView textViewCliente = convertView.findViewById(R.id.tv_apellidos);
        TextView textViewFormaPago = convertView.findViewById(R.id.tv_telefono);

        //adding data to views
        textViewID.setText(client.getIdCliente());
        textViewFecha.setText(client.getNombres());
        /*cliente = cabeceraSQLHelper.getCliente(client.getIdCliente());
        Log.d("Cliente: ", cliente.toString());
        String client = cliente.getNombres()+" "+cliente.getApellidos();*/
        textViewCliente.setText(client.getApellidos());
        //formPago = cabeceraSQLHelper.getFormaPago(client.getIdFormaPago());
        textViewFormaPago.setText(client.getTelefono());

        //we will use these buttons later for update and delete operation
        ImageButton buttonDelete = convertView.findViewById(R.id.buttonDelete);
        ImageButton buttonEdit = convertView.findViewById(R.id.buttonEdit);

        buttonEdit.setTag(position);
        buttonDelete.setTag(position);

        return convertView;
    }



}
