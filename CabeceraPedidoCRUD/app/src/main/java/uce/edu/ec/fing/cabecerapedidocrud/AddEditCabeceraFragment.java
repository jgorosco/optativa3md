package uce.edu.ec.fing.cabecerapedidocrud;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import data.CabeceraSQLHelper;
import data.OperacionesBaseDatos;
import modelo.CabeceraPedido;
import modelo.Cliente;

public class AddEditCabeceraFragment extends Fragment {
    private static final String ARG_CABECERA_ID = "arg_cabecera_id";

    private String cabeceraId;

    private CabeceraSQLHelper baseDatosPedidos;

    private OperacionesBaseDatos operacionesDB;

    private FloatingActionButton mSaveButton;
    public Spinner clientSpinner;
    public Spinner formaPagoSpinner;
    private TextInputLayout clientLabel;
    private TextInputLayout formaPagoLabel;
    private TextInputLayout fechaLabel;
    private String fecha;

    List<Cliente> myList = new ArrayList<Cliente>();

    private final ArrayList<Cliente> clienteArrayList = new ArrayList<Cliente>();

    Button btnDatePicker;
    EditText txtDate;

    public AddEditCabeceraFragment() {
        // Required empty public constructor
    }

    public static AddEditCabeceraFragment newInstance(String lawyerId) {
        AddEditCabeceraFragment fragment = new AddEditCabeceraFragment();
        Bundle args = new Bundle();
        args.putString(ARG_CABECERA_ID, lawyerId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            cabeceraId = getArguments().getString("id_cabecera");
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_add_edit, container, false);

        // Referencias UI
        mSaveButton = (FloatingActionButton) getActivity().findViewById(R.id.fab);
        btnDatePicker=(Button) root.findViewById(R.id.btn_date);
        txtDate=(EditText) root.findViewById(R.id.in_date);
        clientLabel = (TextInputLayout) root.findViewById(R.id.til_client);
        formaPagoLabel = (TextInputLayout) root.findViewById(R.id.til_forma_pago);
        clientSpinner = (Spinner) root.findViewById(R.id.clientspinner);
        formaPagoSpinner = (Spinner) root.findViewById(R.id.formaPagospinner);

        loadClientSpinnerData();
        loadFormaPagoSpinnerData();

        // Carga de datos
        if (cabeceraId != null) {
            loadCabecera();
        }

        return root;
    }


    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                showDatePicker();
            }
        });
    }

    /**
     * Function to load the spinner data from SQLite database
     * */
    private void loadClientSpinnerData() {
        OperacionesBaseDatos db = new OperacionesBaseDatos();
        List<String> labels = db.getAllClients();

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this.getActivity(),android.R.layout.simple_spinner_item, labels);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        clientSpinner.setAdapter(dataAdapter);
    }

    private void loadFormaPagoSpinnerData() {
        OperacionesBaseDatos db = new OperacionesBaseDatos();
        List<String> labels = db.getAllFormaPago();

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this.getActivity(),android.R.layout.simple_spinner_item, labels);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        formaPagoSpinner.setAdapter(dataAdapter);
    }

    private void showDatePicker() {
        DatePickerFragment date = new DatePickerFragment();
        /**
         * Set Up Current Date Into dialog
         */
        Calendar calender = Calendar.getInstance();
        Bundle args = new Bundle();
        args.putInt("year", calender.get(Calendar.YEAR));
        args.putInt("month", calender.get(Calendar.MONTH));
        args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
        date.setArguments(args);
        /**
         * Set Call back to capture selected date
         */
        date.setCallBack(ondate);
        date.show(getFragmentManager(), "Date Picker");
    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            fecha = String.valueOf(dayOfMonth) + "-" + String.format("%02d",monthOfYear+1)
                    + "-" + String.valueOf(year);
            txtDate.setText(fecha);
        }
    };

    private void loadCabecera() {
        new GetCabeceraByIdTask().execute();
    }

    private void showAddEditError() {
        Toast.makeText(getActivity(),
                "Error al agregar nueva información", Toast.LENGTH_SHORT).show();
    }

    private void showCabecera(CabeceraPedido cabeceraPedido) {
            txtDate.setText(cabeceraPedido.getFecha());
    }


    private void showLoadError() {
        Toast.makeText(getActivity(),
                "Error al editar cabecera", Toast.LENGTH_SHORT).show();
    }

    private class GetCabeceraByIdTask extends AsyncTask<Void, Void, CabeceraPedido> {

        @Override
        protected CabeceraPedido doInBackground(Void... voids) {
            return operacionesDB.getCabeceraPedido(cabeceraId);
        }

        @Override
        protected void onPostExecute(CabeceraPedido cursor) {
            if (cursor != null) {
                showCabecera(new CabeceraPedido());
            } else {
                showLoadError();
                getActivity().setResult(Activity.RESULT_CANCELED);
                getActivity().finish();
            }
        }

    }
}
