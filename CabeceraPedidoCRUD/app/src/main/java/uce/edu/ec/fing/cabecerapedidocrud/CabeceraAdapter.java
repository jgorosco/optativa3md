package uce.edu.ec.fing.cabecerapedidocrud;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import data.CabeceraSQLHelper;
import data.ContratoPedidos;
import data.OperacionesBaseDatos;
import modelo.CabeceraPedido;
import modelo.Cliente;
import modelo.FormaPago;

public class CabeceraAdapter extends ArrayAdapter<CabeceraPedido> {

    Context mCtx;
    int listLayoutRes;
    List<CabeceraPedido> cabeceraList;
    OperacionesBaseDatos datos;

    public CabeceraAdapter(Context context, List<CabeceraPedido> objects) {super(context,0, objects);}


    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //getting employee of the specified position
        CabeceraPedido cabecera = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_layout_cabecera, parent, false);
        }

        //getting views
        TextView textViewID = (TextView) convertView.findViewById(R.id.tv_id_cabecera);
        TextView textViewFecha = (TextView) convertView.findViewById(R.id.tv_fecha);
        TextView textViewCliente = (TextView) convertView.findViewById(R.id.tv_cliente);
        TextView textViewFormaPago = (TextView) convertView.findViewById(R.id.tv_formapago);

        //adding data to views
        textViewID.setText(cabecera.getIdCabeceraPedido());
        textViewFecha.setText(cabecera.getFecha());
        /*cliente = cabeceraSQLHelper.getCliente(cabecera.getIdCliente());
        Log.d("Cliente: ", cliente.toString());
        String client = cliente.getNombres()+" "+cliente.getApellidos();*/
        textViewCliente.setText(cabecera.getIdCliente());
        //formPago = cabeceraSQLHelper.getFormaPago(cabecera.getIdFormaPago());
        textViewFormaPago.setText(cabecera.getIdFormaPago());

        //we will use these buttons later for update and delete operation
        ImageButton buttonDelete = (ImageButton) convertView.findViewById(R.id.buttonDelete);
        ImageButton buttonEdit = (ImageButton) convertView.findViewById(R.id.buttonEdit);

        buttonEdit.setTag(position);
        buttonDelete.setTag(position);

        return convertView;
    }



}
