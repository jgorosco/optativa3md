package uce.edu.ec.fing.cabecerapedidocrud;


import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.activity.ComponentActivity;
import androidx.activity.OnBackPressedDispatcher;
import androidx.annotation.MainThread;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import data.OperacionesBaseDatos;
import modelo.CabeceraPedido;
import modelo.Cliente;
import modelo.FormaPago;

public class AddEditActivity extends AppCompatActivity {

    private String id_cabecera = null;

    public static final int REQUEST_ADD_CABECERA = 1;

    private String fecha;
    private Cliente cliente =  new Cliente();
    private FormaPago formaPago;
    Spinner clientSpinner;
    Spinner formaPagoSpinner;
    EditText datetex;

    OperacionesBaseDatos datos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_edit_activity);


        id_cabecera = getIntent().getStringExtra("id_cabecera");

        setTitle(id_cabecera == null ? "Añadir cabecera" : "Editar cabecera");

        AddEditCabeceraFragment addEditPedidoFragment = (AddEditCabeceraFragment)getSupportFragmentManager().findFragmentById(R.id.add_edit_cabecera_container);
        if (addEditPedidoFragment == null) {
            addEditPedidoFragment = AddEditCabeceraFragment.newInstance(id_cabecera);
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.add_edit_cabecera_container, addEditPedidoFragment)
                    .commit();
        }

        datos = OperacionesBaseDatos
                .obtenerInstancia(getApplicationContext());


    }

    public String get_Time(String fecha) {
        int anio,mes,dia;
        Calendar date;
        String[] dateParts = fecha.split("-");
        dia = Integer.parseInt(dateParts[0]);
        mes = Integer.parseInt(dateParts[1])-1;
        anio = Integer.parseInt(dateParts[2]);
        date = new GregorianCalendar(anio,mes,dia);
        String CURRENT_TIME_FORMAT = "yyyy MMMM dd";
        Locale local = new Locale("es", "EC");
        SimpleDateFormat dateFormat = new SimpleDateFormat(CURRENT_TIME_FORMAT, local);
        return dateFormat.format(date.getTime());
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab_add:
                datetex = findViewById(R.id.in_date);
                clientSpinner = findViewById(R.id.clientspinner);
                formaPagoSpinner = findViewById(R.id.formaPagospinner);
                fecha = get_Time(datetex.getText().toString());
                cliente = datos.getCliente(clientSpinner.getSelectedItem().toString());
                formaPago = datos.getFormaPago(formaPagoSpinner.getSelectedItem().toString());
                if(id_cabecera!=null){
                    CabeceraPedido cp = datos.getCabeceraPedido(id_cabecera);
                    cp.setFecha(fecha);
                    cp.setIdCliente(cliente.getIdCliente());
                    cp.setIdFormaPago(formaPago.getIdFormaPago());
                    datos.actualizarCabeceraPedido(cp);
                    Toast.makeText(this, "Cabecera Actualizada Exitosamente", Toast.LENGTH_SHORT).show();
                }else {
                    CabeceraPedido cp = new CabeceraPedido(null, fecha, cliente.getIdCliente(), formaPago.getIdFormaPago());
                    datos.insertarCabeceraPedido(cp);
                    Toast.makeText(this, "Cabecera Añadida Exitosamente", Toast.LENGTH_SHORT).show();
                }
                break;
            /*case R.id.buttonEdit:
                datetex = findViewById(R.id.in_date);
                clientSpinner = findViewById(R.id.clientspinner);
                formaPagoSpinner = findViewById(R.id.formaPagospinner);
                fecha = get_Time(datetex.getText().toString());
                cliente = datos.getCliente(clientSpinner.getSelectedItem().toString());
                formaPago = datos.getFormaPago(formaPagoSpinner.getSelectedItem().toString());
                CabeceraPedido cp = datos.getCabeceraPedido(id_cabecera);
                cp.setFecha(fecha);
                cp.setIdCliente(cliente.getIdCliente());
                cp.setIdFormaPago(formaPago.getIdFormaPago());
                datos.actualizarCabeceraPedido(cp);
                Toast.makeText(this, "Cabecera Actualizada Exitosamente", Toast.LENGTH_SHORT).show();
                break;*/
        }
    }


}
