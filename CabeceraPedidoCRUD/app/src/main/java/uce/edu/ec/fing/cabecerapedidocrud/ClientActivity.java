package uce.edu.ec.fing.cabecerapedidocrud;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageButton;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.List;

import data.OperacionesBaseDatos;
import modelo.CabeceraPedido;
import modelo.Cliente;

public class ClientActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{

    List<Cliente> clienteList;
    ListView listViewCliente;
    ClienteAdapter adapter;
    OperacionesBaseDatos datos;
    ImageButton btnEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.client_activity);

        Toolbar toolbar = findViewById(R.id.toolbar_cliente);
        setSupportActionBar(toolbar);


        listViewCliente = findViewById(R.id.list_item);
        btnEdit = findViewById(R.id.buttonEdit);

        clienteList = new ArrayList<>();

        datos = OperacionesBaseDatos
                .obtenerInstancia(getApplicationContext());

        //this method will display the employees in the list
        shoeClienteFromDatabase();
    }

    private void shoeClienteFromDatabase() {

        //we used rawQuery(sql, selectionargs) for fetching all the employees
        Cursor cursorEmployees = datos.obtenerClientes();

        //if the cursor has some data
        if (cursorEmployees.moveToFirst()) {
            //looping through all the records
            do {
                //pushing each record in the employee list
                clienteList.add(new Cliente(
                        cursorEmployees.getString(0),
                        cursorEmployees.getString(1),
                        cursorEmployees.getString(2),
                        cursorEmployees.getString(3)
                ));
            } while (cursorEmployees.moveToNext());
        }
        //closing the cursor
        cursorEmployees.close();

        //creating the adapter object
        adapter = new ClienteAdapter(this, clienteList);

        //adding the adapter to listview
        listViewCliente.setAdapter(adapter);

    }

    //Cuando doy click en un item del menu, voy mostrando los layouts de acuerdo al item seleccionado.
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_cliente:
                Intent intent = new Intent(this, ClientActivity.class);
                startActivity(intent);
            default:

        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}

