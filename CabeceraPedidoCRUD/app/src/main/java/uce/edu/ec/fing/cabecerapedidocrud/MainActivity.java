package uce.edu.ec.fing.cabecerapedidocrud;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import data.CabeceraSQLHelper;
import data.ContratoPedidos;
import data.OperacionesBaseDatos;
import modelo.CabeceraPedido;
import modelo.Cliente;
import modelo.FormaPago;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static final int REQUEST_UPDATE_DELETE_LAWYER = 2;

        List<CabeceraPedido> cabeceraList;
        SQLiteDatabase mDatabase;
        ListView listViewCabecera;
        CabeceraAdapter adapter;
        OperacionesBaseDatos datos;
        ImageButton btnEdit;

        int position;


        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            Toolbar toolbar = findViewById(R.id.toolbar_cabecera);
            setSupportActionBar(toolbar);


            listViewCabecera = findViewById(R.id.list_item);
            btnEdit = findViewById(R.id.buttonEdit);

            cabeceraList = new ArrayList<>();

            getApplicationContext().deleteDatabase("Cabecera.db");

            datos = OperacionesBaseDatos
                    .obtenerInstancia(getApplicationContext());

            new TareaPruebaDatos().execute();

            //this method will display the employees in the list
            showCabeceraFromDatabase();
        }

    public String get_current_Time() {
        String CURRENT_TIME_FORMAT = "yyyy MMMM dd";
        Locale local = new Locale("es", "EC");
        SimpleDateFormat dateFormat = new SimpleDateFormat(CURRENT_TIME_FORMAT, local);
        return dateFormat.format(Calendar.getInstance().getTime());
    }

    public class TareaPruebaDatos extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {

            // [INSERCIONES]
            String fechaActual = get_current_Time();

            try {

                datos.getDb().beginTransaction();

                // Inserción Clientes
                String cliente1 = datos.addCliente(new Cliente(null, "Veronica", "Del Topo", "4552000"));
                String cliente2 = datos.addCliente(new Cliente(null, "Carlos", "Villagran", "4440000"));
                String cliente3 = datos.addCliente(new Cliente(null, "Mario", "Paredes", "3123849"));
                String cliente4 = datos.addCliente(new Cliente(null, "Pedro", "Lopez", "2933288"));
                String cliente5 = datos.addCliente(new Cliente(null, "Sofia", "Martínez", "2348328"));
                String cliente6 = datos.addCliente(new Cliente(null, "Elias", "Moreno", "2323429"));
                String cliente7 = datos.addCliente(new Cliente(null, "Gabriel", "Orosco", "3428829"));
                String cliente8 = datos.addCliente(new Cliente(null, "Wendy", "Gaona", "3193499"));
                String cliente9 = datos.addCliente(new Cliente(null, "Maria", "Jiménez", "2383289"));
                String cliente10 = datos.addCliente(new Cliente(null, "Jaime", "Flores", "2035930"));

                // Inserción Formas de pago
                String formaPago1 = datos.addFormaPago(new FormaPago(null, "Efectivo"));
                String formaPago2 = datos.addFormaPago(new FormaPago(null, "Crédito"));
                String formaPago3 = datos.addFormaPago(new FormaPago(null, "Cheque"));

                // Inserción CabeceraPedidos
                String cabecera1 = datos.addCabecera(
                        new CabeceraPedido(null, fechaActual, cliente1, formaPago1));
                String cabecera2 = datos.addCabecera(
                        new CabeceraPedido(null, fechaActual,cliente2, formaPago2));


                datos.getDb().setTransactionSuccessful();

            } finally {
                datos.getDb().endTransaction();
            }

            // [QUERIES]
            return null;
        }
    }


    private void showCabeceraFromDatabase() {

        //we used rawQuery(sql, selectionargs) for fetching all the employees
        Cursor cursorEmployees = datos.obtenerCabecerasPedidos();

        //if the cursor has some data
        if (cursorEmployees.moveToFirst()) {
            //looping through all the records
            do {
                //pushing each record in the employee list
                cabeceraList.add(new CabeceraPedido(
                        cursorEmployees.getString(0),
                        cursorEmployees.getString(1),
                        cursorEmployees.getString(2),
                        cursorEmployees.getString(3)
                ));
            } while (cursorEmployees.moveToNext());
        }
        //closing the cursor
        cursorEmployees.close();

           //creating the adapter object
            adapter = new CabeceraAdapter(this, cabeceraList);

        //adding the adapter to listview
            listViewCabecera.setAdapter(adapter);


        }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab:
                showAddScreen();
                break;
            case R.id.buttonDelete:
                int positionD = (Integer) view.getTag();
                CabeceraPedido currentItemDel = adapter.getItem(positionD);
                String currentCabeceraIDel = currentItemDel.getIdCabeceraPedido();
                confirmDelete(currentCabeceraIDel);
                break;
            case R.id.buttonEdit:
                int positionE = (Integer) view.getTag();
                CabeceraPedido currentItemEdit = adapter.getItem(positionE);
                String currentCabeceraIDedit = currentItemEdit.getIdCabeceraPedido();
                showEditScreen(currentCabeceraIDedit);
                break;
        }
    }

    public void confirmDelete(String id){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Eliminar");
        builder.setMessage("Está Seguro?");

        builder.setPositiveButton("SI", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Eliminar Cabecera
                datos.eliminarCabeceraPedido(id);
            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // No hacer nada
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                onResume();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        //if the data has changed
        this.cabeceraList.clear();
        showCabeceraFromDatabase();
        adapter.notifyDataSetChanged();
    }

    private void showAddScreen() {
        Intent intent = new Intent(this, AddEditActivity.class);
        startActivityForResult(intent, AddEditActivity.REQUEST_ADD_CABECERA);
    }

    private void showEditScreen(String cabeceraId){
        Intent intent = new Intent(getBaseContext(), AddEditActivity.class);
        intent.putExtra("id_cabecera", cabeceraId);
        startActivity(intent);
    }

    //Cuando doy click en un item del menu, voy mostrando los layouts de acuerdo al item seleccionado.
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_cliente:
                Intent intent = new Intent(this, ClientActivity.class);
                startActivity(intent);
            default:

        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


}